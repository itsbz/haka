import { Module } from '@nestjs/common'
import { Config, DB } from './settings'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { UsersModule } from '../users/users.module'
import { AuthModule } from '@/modules/auth/auth.module'

@Module({
	imports: [Config, DB, UsersModule, AuthModule],
	controllers: [AppController],
	providers: [AppService],
})
export class AppModule {}
