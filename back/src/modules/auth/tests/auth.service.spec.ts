import { Test, TestingModule } from '@nestjs/testing'
import { AuthService } from '../auth.service'
import { AuthServiceMock } from '@/modules/auth/tests/auth.service.mock'

describe('AuthService', () => {
	let service: AuthService

	beforeEach(async () => {
		const module: TestingModule = await Test.createTestingModule({
			providers: [AuthServiceMock],
		}).compile()

		service = module.get<AuthService>(AuthService)
	})

	it('should be defined', () => {
		expect(service).toBeDefined()
	})
})
